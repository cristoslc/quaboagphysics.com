. ~/cronjobs/_setGlobals.sh "$1"

# set site-specific info
_site="quaboagphysics.sites.cristoslc.com"
_sqlHost=""
_sqlDb=""
_sqlUser=""
_sqlPass=""


. ~/cronjobs/_setLocals.sh

. ~/cronjobs/_runCommands.sh
